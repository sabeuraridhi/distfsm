#!/usr/bin/env perl
$nbpart=@ARGV[0];

sub GetFilesList
{
        my $Path = $_[0];
        my $FileFound;
        my @FilesList=();

        # Lecture de la liste des fichiers
        opendir (my $FhRep, $Path)
                or die "Impossible d'ouvrir le repertoire $Path\n";
        my @Contenu = grep { !/^\.\.?$/ } readdir($FhRep);
        closedir ($FhRep);

        foreach my $FileFound (@Contenu) {
                # Traitement des fichiers
                if ( -f "$Path/$FileFound") {
                        push ( @FilesList, "$Path/$FileFound" );
                }
                # Traitement des repertoires
                elsif ( -d "$Path/$FileFound") {
                        # Boucle pour lancer la recherche en mode recursif
                        push (@FilesList, GetFilesList("$Path/$FileFound") );
                }

        }
        return @FilesList;
}

# Construire les subpartitions : chaque bucket est divisé en n sous-partitions
`rm -r buckets/subpart`; 
`mkdir buckets/subpart`;
@ListFiles = GetFilesList ("buckets");
$nbbuckets=scalar(@ListFiles);
$count=0;
print "nb buckets = $nbbuckets\n";
foreach my $file  (@ListFiles) {
$count++;
`rm -r buckets/subpart/$count`; 
`mkdir buckets/subpart/$count`;
$nbgraphs=`cat $file | wc -l`;
print "nb graphs in the buckets $file = $nbgraphs\n";
$nbline=int($nbgraphs/$nbpart)+1;
`split -l $nbline $file buckets/subpart/$count/new`;
}

# construire les chunks
	for ($i=1; $i<$nbbuckets+1; $i++)
	{ 
		$nbpartcount=0;
		@ListSubpart = GetFilesList ("buckets/subpart/$i");
		foreach my $file  (@ListSubpart) {
		print "Appending $file into Part$nbpartcount\n";
		`cat $file >> Part$nbpartcount`;
		$nbpartcount++;

		}
		print "$i\n";
	}


