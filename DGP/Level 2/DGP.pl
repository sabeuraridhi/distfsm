#!/usr/bin/env perl
$nbparts=@ARGV[0];
$buckets=@ARGV[1];
$ds=@ARGV[2];


`rm Part*`;
$nbgraphs=`cat $ds | wc -l`;

`./Decomposition.pl $ds $buckets $nbgraphs`;
`./Distribution.pl $nbparts`;

for($i=0;$i<$nbparts;$i++)
{
`/usr/local/hadoop/bin/hadoop dfs -copyFromLocal Part$i buckets$buckets/DGP_$ds/Part$i`;
}

